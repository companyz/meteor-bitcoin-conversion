if (Meteor.isClient) {

  ComputeResult = function(amount, rate) {
    var result = (amount/rate).toFixed(10);
    Session.set('conversion_result', result);
  }

  Template.hello.helpers({
    ticker: function() {
      return ticker.get();
    }
  });

  Template.hello.created = function () {
    Session.set('conversion_result', "");
    ticker = new ReactiveVar();
    Meteor.call("callTicker", function(err, result) {
      var r = []
      _.each(result.data, function(value, key, list) {
        r.push(_.extend(value, {"currency" : key}));
      });
      ticker.set(_.toArray(r));
    });
  };

  Template.converter.helpers({
    ticker: function () {
      Template.parentData();
    },

    btcSelected: function() {
      return $('#currency_from').val() == "BTC";
    },

    conversionResult: function() {
      return Session.get('conversion_result') ? Session.get('conversion_result') : "";
    }

  });

  Template.converter.events({
    'keyup #conversion_amount': function (event) {
      var amount = event.target.value;
      var conversionRate = $("#currency_from option:selected").attr('data-rate');
      return ComputeResult(amount, conversionRate)
    },

    'change #currency_from': function(event) {
      var amount = parseFloat($("#conversion_amount").val(), 10);
      console.log(amount);
      var conversionRate = $("#currency_from option:selected").attr('data-rate');
      console.log(conversionRate);
      return ComputeResult(amount, conversionRate)
    }
  });
}

if (Meteor.isServer) {

  Meteor.methods({
    callTicker: function() {
      return Meteor.http.get("http://www.blockchain.info/ticker");
    }
  });
}
